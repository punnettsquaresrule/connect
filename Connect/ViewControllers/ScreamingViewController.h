//
//  ScreamingViewController.h
//  Connect
//
//  Created by Niels de Hoog on 6/6/13.
//  Copyright (c) 2013 Damn Imagination. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RemoteDevice;

@interface ScreamingViewController : UIViewController

- (id)initWithRemoteDevice:(RemoteDevice *)remoteDevice;

@end
