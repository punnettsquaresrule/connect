//
//  ScreamingViewController.m
//  Connect
//
//  Created by Niels de Hoog on 6/6/13.
//  Copyright (c) 2013 Damn Imagination. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#import "ScreamingViewController.h"

#import "DeviceBrowser.h"
#import "RemoteDevice.h"

// the lower the more smooth
#define SMOOTHING_FACTOR 0.5f

// recording time in seconds
#define RECORDING_TIME 3

// noise threshold to reach
#define NOISE_THRESHOLD 0.6

@interface ScreamingViewController () <RemoteDeviceDelegate>
{
    // recording
    float _lowPassResult;
    BOOL _isRecording;
}

@property (nonatomic, strong) RemoteDevice *remoteDevice;
@property (nonatomic, strong) DeviceBrowser *deviceBrowser;

@property (nonatomic, strong) IBOutlet UILabel *statusLabel;
@property (nonatomic, strong) IBOutlet UIProgressView *levelMeter;

@property (nonatomic, strong) AVAudioRecorder *recorder;
@property (nonatomic, strong) NSTimer *updateTimer;

@property (nonatomic, strong) NSMutableSet *recordingValues;
@property (nonatomic, strong) NSNumber *localAverage;

@end

@implementation ScreamingViewController

- (id)initWithRemoteDevice:(RemoteDevice *)remoteDevice
{
    self = [super init];
    if (self) {
        _remoteDevice = remoteDevice;
        _remoteDevice.delegate = self;
    }
    return self;
}

- (void)dealloc
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.remoteDevice) {
        self.deviceBrowser = [[DeviceBrowser alloc] init];
        
        self.statusLabel.text = @"Searching for devices...";
        
        [self.deviceBrowser findRemoteDeviceWithCompletion:^(RemoteDevice *device) {
            
            self.statusLabel.text = @"Found device. Waiting for confirmation...";
            
            self.remoteDevice = device;
            self.remoteDevice.delegate = self;
            
            if (![self.remoteDevice connect]) {
                DLog(@"failed to connect!");
            }

        } failure:^(NSError *error) {
            
            self.statusLabel.text = @"Failed to find another device";
        }];
    }
    else {
        self.statusLabel.text = @"Session started on remote device";
    }
}

#pragma mark - Button actions

- (IBAction)closeButtonPressed:(id)sender
{
    // stop recording
    [self stopRecording];
    
    // close connection
    [self.remoteDevice closeConnection];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - RemoteDevice delegate

- (void)deviceDidCancel:(RemoteDevice *)device
{
    [self stopRecording];
    
    self.statusLabel.text = @"Device cancelled";
    
    self.remoteDevice = nil;
}

- (void)deviceDidAcceptChallenge:(RemoteDevice *)device
{
    self.statusLabel.text = @"Device did accept challenge";
    
    [self.remoteDevice sendStartRecording];
    [self startRecording];
}

- (void)deviceWillStartRecording:(RemoteDevice *)device
{
    [self startRecording];
}

- (void)deviceDidReportAverageNoiseValue:(NSNumber *)average
{
    float globalAverage = ([average floatValue] + [self.localAverage floatValue]) / 2.0;
        
    if (globalAverage >= NOISE_THRESHOLD) {
        self.statusLabel.text = @"Successed!";
    }
    else {
        self.statusLabel.text = @"Not loud enough!";
    }
}

#pragma mark - Recording

- (AVAudioRecorder *)recorder
{
    if (!_recorder) {
        NSDictionary *recorderSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithFloat:44100.0],                 AVSampleRateKey,
                                  [NSNumber numberWithInt:kAudioFormatAppleLossless], AVFormatIDKey,
                                  [NSNumber numberWithInt:1],                         AVNumberOfChannelsKey,
                                  [NSNumber numberWithInt:AVAudioQualityLow],         AVEncoderAudioQualityKey,
                                  nil];
        NSError* error = nil;
        _recorder = [[AVAudioRecorder alloc] initWithURL:[NSURL URLWithString:[NSTemporaryDirectory() stringByAppendingPathComponent:@"tmp.caf"]]  settings:recorderSettings error:&error];
        
        _recorder.meteringEnabled = YES;
    }
    
    return _recorder;
}

- (void)startRecording
{
    if (_isRecording) {
        return;
    }
    
    _isRecording = YES;
    
    _lowPassResult = 0;
    self.recordingValues = [NSMutableSet set];
    
    [self.recorder record];
    
    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(updateMeters) userInfo:nil repeats:YES];
    
    // executed after recording time is over
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, RECORDING_TIME * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [self sendResults];
        
        [self stopRecording];
    });
}

- (void)stopRecording
{
    if (!_isRecording) {
        return;
    }
    
    _isRecording = NO;
    
    [self.recorder stop];
    [self.recorder deleteRecording];
    
    [self.updateTimer invalidate];
    self.updateTimer = nil;
    
    self.recordingValues = nil;
    
    self.statusLabel.text = @"Waiting for results...";
}

- (void)updateMeters
{
    [self.recorder updateMeters];
    
	double averagePower = pow(10, (0.05 * [self.recorder averagePowerForChannel:0]));
	_lowPassResult = SMOOTHING_FACTOR * averagePower + (1.0 - SMOOTHING_FACTOR) * _lowPassResult;
    
    self.statusLabel.text = [NSString stringWithFormat:@"Peak power: %.2f, lowPass: %.2f", averagePower, _lowPassResult];
    
    self.levelMeter.progress = _lowPassResult;
    
    [self.recordingValues addObject:@(_lowPassResult)];
}

- (void)sendResults
{
    NSNumber *average = [self.recordingValues valueForKeyPath:@"@avg.floatValue"];
    self.localAverage = average;

    [self.remoteDevice sendAverageNoiseValue:average];
}

@end
