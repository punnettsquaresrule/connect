//
//  MainViewController.m
//  Connect
//
//  Created by Niels de Hoog on 6/4/13.
//  Copyright (c) 2013 Damn Imagination. All rights reserved.
//

#import "MainViewController.h"
#import "ScreamingViewController.h"

#import "RemoteDevice.h"
#import "ServerManager.h"

@interface MainViewController ()


@end

@implementation MainViewController

- (id)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedChallengeNotificationReceived:) name:kDidAcceptChallengeNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)presentScreamingViewControllerWithRemoteDevice:(RemoteDevice *)remoteDevice
{
    ScreamingViewController *screamViewController = [[ScreamingViewController alloc] initWithRemoteDevice:remoteDevice];
    [self presentViewController:screamViewController animated:YES completion:nil];
}

#pragma mark - Notifications

- (void)acceptedChallengeNotificationReceived:(NSNotification *)notification
{
    if (!self.presentedViewController) {
        RemoteDevice *remoteDevice = [[notification userInfo] objectForKey:@"device"];
        [self presentScreamingViewControllerWithRemoteDevice:remoteDevice];
    }
}

#pragma mark - Button actions

- (IBAction)openSceneButtonPressed:(UIButton *)button
{
    [self presentScreamingViewControllerWithRemoteDevice:nil];
}

@end
