//
//  DeviceBrowser.h
//  Connect
//
//  Created by Niels de Hoog on 6/5/13.
//  Copyright (c) 2013 Damn Imagination. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RemoteDevice;

@interface DeviceBrowser : NSObject

// find another device through bluetooth (WiFi for debugging)
- (void)findRemoteDeviceWithCompletion:(void (^)(RemoteDevice *device))completion
                               failure:(void (^)(NSError *error))failure;

@end
