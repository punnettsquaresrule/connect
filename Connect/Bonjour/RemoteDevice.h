//
//  RemoteDevice.h
//  Connect
//
//  Created by Niels de Hoog on 6/5/13.
//  Copyright (c) 2013 Damn Imagination. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Connection;

@protocol RemoteDeviceDelegate;

@interface RemoteDevice : NSObject

@property (nonatomic, weak) id<RemoteDeviceDelegate> delegate;

// designated initializer
- (id)initWithConnection:(Connection *)connection;

// set up connection with remote device
- (BOOL)connect;
- (void)closeConnection;

// notify the remote device that this device accepts the challenge
- (void)sendAcceptChallenge;

// notify the remote device that this device will start recording
- (void)sendStartRecording;

// send average recorded noise value to remote device. (value between 0 and 1)
- (void)sendAverageNoiseValue:(NSNumber *)average;

@end

@protocol RemoteDeviceDelegate <NSObject>

- (void)deviceDidAcceptChallenge:(RemoteDevice *)device;

- (void)deviceWillStartRecording:(RemoteDevice *)device;

- (void)deviceDidReportAverageNoiseValue:(NSNumber *)average;

- (void)deviceDidCancel:(RemoteDevice *)device;

@end
