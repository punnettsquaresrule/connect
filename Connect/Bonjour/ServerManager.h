//
//  ServerManager.h
//  Connect
//
//  Created by Niels de Hoog on 6/6/13.
//  Copyright (c) 2013 Damn Imagination. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const kDidAcceptChallengeNotification;

@interface ServerManager : NSObject

// the name of the current device on bonjour. available after succesful bonjour registration
@property (nonatomic, readonly) NSString *registrationName;

+ (ServerManager *)sharedManager;

// start listening for connections and advertise with bonjour
- (void)startServer;

// stop listening for connections and stop advertising with bonjour
- (void)stopServer;

@end
