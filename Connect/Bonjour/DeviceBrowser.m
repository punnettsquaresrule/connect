//
//  DeviceBrowser.m
//  Connect
//
//  Created by Niels de Hoog on 6/5/13.
//  Copyright (c) 2013 Damn Imagination. All rights reserved.
//

#import "DeviceBrowser.h"

#import "DNSSDBrowser.h"
#import "DNSSDService.h"

#import "Connection.h"
#import "RemoteDevice.h"

#import "ServerManager.h"

@interface DeviceBrowser () <DNSSDBrowserDelegate, DNSSDServiceDelegate>

@property (nonatomic, copy) void (^completionBlock)(RemoteDevice *device);
@property (nonatomic, copy) void (^failureBlock)(NSError *error);

@property (nonatomic, strong) DNSSDBrowser *browser;
@property (nonatomic, strong) DNSSDService *service;

@property (nonatomic, strong) NSTimer *browsingTimer;

@end

@implementation DeviceBrowser

static NSString * const kDeviceBrowserErrorDomain = @"kDeviceBrowserErrorDomain";

- (id)init
{
    self = [super init];
    if (self) {
        _browser = [[DNSSDBrowser alloc] initWithDomain:@"" type:BONJOUR_SERVICE_IDENTIFIER];
        _browser.delegate = self;
        
#ifndef WIFI
        _browser.useP2P = YES;
#endif
    }
    return self;
}

- (void)findRemoteDeviceWithCompletion:(void (^)(RemoteDevice *device))completion
                               failure:(void (^)(NSError *error))failure
{
    self.completionBlock = completion;
    self.failureBlock = failure;
    
    [self.browser startBrowse];
    
    self.browsingTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(browsingTimerFired:) userInfo:nil repeats:NO];
}

#pragma mark - Timer

- (void)browsingTimerFired:(NSTimer *)timer
{
    
    [self.browser stop];
    self.browsingTimer = nil;
    
    NSError *error = [NSError errorWithDomain:kDeviceBrowserErrorDomain code:404 userInfo:@{NSLocalizedDescriptionKey:@"Time out bij het zoeken naar devices"}];
        
    if (self.failureBlock) {
        _failureBlock(error);
    }
}

- (void)cancelBrowsingTimer
{
    [self.browsingTimer invalidate];
    self.browsingTimer = nil;
}

#pragma mark - Helpers

- (void)resolveService:(DNSSDService *)service
{
    self.service = service;
    self.service.delegate = self;
    
#ifndef WIFI
    self.service.useP2P = YES;
#endif
    
    [self.service startResolve];
}

#pragma mark - DNSSDBrowser delegate

- (void)dnssdBrowser:(DNSSDBrowser *)browser didAddService:(DNSSDService *)service moreComing:(BOOL)moreComing
{
    NSString *deviceName = [[ServerManager sharedManager] registrationName];
    if ([service.name isEqualToString:deviceName]) {
        DLog(@"found itself :/");
        return;
    }
    
    DLog(@"did add service with name: %@", service.name);
    
    [self resolveService:service];
    
    [self.browser stop];
    
    [self cancelBrowsingTimer];
}

- (void)dnssdBrowser:(DNSSDBrowser *)browser didNotBrowse:(NSError *)error
{
    DLog(@"failed to browse: %@", error);
    
    [self cancelBrowsingTimer];
    [self.browser stop];
    
    if (self.failureBlock) {
        _failureBlock(error);
    }
}

#pragma mark - DNSSDService delegate

- (void)dnssdServiceDidResolveAddress:(DNSSDService *)service
{
    DLog(@"did resolve to host: %@:%d", service.resolvedHost, service.resolvedPort);
    
    if (self.completionBlock) {
        
        Connection *connection = [[Connection alloc] initWithHostAddress:service.resolvedHost andPort:service.resolvedPort];
        RemoteDevice *device = [[RemoteDevice alloc] initWithConnection:connection];
        
        _completionBlock(device);
    }
}

- (void)dnssdService:(DNSSDService *)service didNotResolve:(NSError *)error
{
    DLog(@"did not resolve: %@", error);
    
    [self.browser stop];
    
    if (self.failureBlock) {
        _failureBlock(error);
    }
}

@end
