//
//  RemoteDevice.m
//  Connect
//
//  Created by Niels de Hoog on 6/5/13.
//  Copyright (c) 2013 Damn Imagination. All rights reserved.
//

#import "RemoteDevice.h"
#import "Connection.h"

@interface RemoteDevice () <ConnectionDelegate>

@property (nonatomic, strong) Connection *connection;

@end

@implementation RemoteDevice

static NSString * const kAcceptChallengeKey = @"kAcceptChallengeKey";
static NSString * const kStartRecordingKey = @"kStartRecordingKey";
static NSString * const kAverageNoiseValueKey = @"kAverageNoiseValueKey";

- (id)initWithConnection:(Connection *)connection
{
    self = [super init];
    if (self) {
        _connection = connection;
        _connection.delegate = self;
    }
    
    return self;
}

- (void)dealloc
{
    [self.connection close];
}

- (BOOL)connect
{
    return [self.connection connect];
}

- (void)closeConnection
{
    [self.connection close];
}

#pragma mark - Communication

- (void)sendAcceptChallenge
{
    [self.connection sendNetworkPacket:@{kAcceptChallengeKey: @(YES)}];
}

- (void)sendStartRecording
{
    [self.connection sendNetworkPacket:@{kStartRecordingKey: @(YES)}];
}

- (void)sendAverageNoiseValue:(NSNumber *)average;
{
    [self.connection sendNetworkPacket:@{kAverageNoiseValueKey: average}];
}

#pragma mark - Connection delegate

- (void)connectionAttemptFailed:(Connection *)connection
{
    DLog(@"connection failed: %@", connection);
    [self.delegate deviceDidCancel:self];
}

- (void)connectionTerminated:(Connection *)connection
{
    DLog(@"connection terminated: %@", connection);
    [self.delegate deviceDidCancel:self];
}

- (void)receivedNetworkPacket:(NSDictionary *)message viaConnection:(Connection *)connection
{
    if (connection != self.connection) {
        DLog(@"receiving message from wrong connection");
        return;
    }
    
    DLog(@"received message: %@", message);
    
    if ([message objectForKey:kAcceptChallengeKey]) {
        [self.delegate deviceDidAcceptChallenge:self];
    }
    else if ([message objectForKey:kStartRecordingKey]) {
        [self.delegate deviceWillStartRecording:self];
    }
    else if ([message objectForKey:kAverageNoiseValueKey]) {
        [self.delegate deviceDidReportAverageNoiseValue:[message objectForKey:kAverageNoiseValueKey]];
    }
    else if ([message objectForKey:@"message"]) {
        [[[UIAlertView alloc] initWithTitle:@"Message" message:[message objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }
}

@end
