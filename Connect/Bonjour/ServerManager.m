//
//  ServerManager.m
//  Connect
//
//  Created by Niels de Hoog on 6/6/13.
//  Copyright (c) 2013 Damn Imagination. All rights reserved.
//

#import "ServerManager.h"
#import "Server.h"
#import "RemoteDevice.h"

#define CANCEL_BUTTON_INDEX 0
#define CONFIRM_BUTTON_INDEX 1

NSString * const kDidAcceptChallengeNotification = @"kDidAcceptChallengeNotification";

@interface ServerManager () <ServerDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) Server *server;

@property (nonatomic, strong) RemoteDevice *remoteDevice;

@end

@implementation ServerManager

+ (ServerManager *)sharedManager
{
    static ServerManager *__sharedInstance = nil;
    static dispatch_once_t __onceToken;
    dispatch_once(&__onceToken, ^{
        __sharedInstance = [[self alloc] init];
    });
    return __sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.server = [[Server alloc] initWithDomain:@"" type:BONJOUR_SERVICE_IDENTIFIER];
        self.server.delegate = self;
    }
    return self;
}

#pragma mark - Server

- (void)startServer
{
    [self.server start];
}

- (void)stopServer
{
    [self.server stop];
}

- (NSString *)registrationName
{
    return [self.server registrationName];
}

#pragma mark - Server delegate

- (void)serverDidStart:(Server *)server
{
    DLog(@"server did start: %@", server);
}

- (void)serverFailed:(Server *)server error:(NSError *)error
{
    DLog(@"server failed with error: %@", error);
}

- (void)server:(Server *)server didReceiveNewConnection:(Connection *)connection
{
    DLog(@"did receive new connection: %@", connection);
    
    self.remoteDevice = [[RemoteDevice alloc] initWithConnection:connection];
        
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Incoming connection" message:@"Do you wish to allow this incoming connection?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [alertView show];
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == CANCEL_BUTTON_INDEX) {
        // this will close the connection
        self.remoteDevice = nil;
    }
    else if (buttonIndex == CONFIRM_BUTTON_INDEX) {
        [self.remoteDevice sendAcceptChallenge];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidAcceptChallengeNotification object:self userInfo:@{@"device": self.remoteDevice}];
        
        self.remoteDevice = nil;
    }
}

@end
