//
//  AppDelegate.h
//  Connect
//
//  Created by Niels de Hoog on 6/4/13.
//  Copyright (c) 2013 Damn Imagination. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
