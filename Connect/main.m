//
//  main.m
//  Connect
//
//  Created by Niels de Hoog on 6/4/13.
//  Copyright (c) 2013 Damn Imagination. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
